# TODO
- [ ] #5 Add common function for two teste's 'test_reading_from_not_existed_file_must_return_empty_vector' and 'test_reading_from_folder_must_return_empty_vector'
- [ ] #6 - Add unit test with empty lines passing to Blocks::parse()
- [ ] #7 Add unit test for passing zero count blocks to Tickets::parse()
- [ ] #8 Remove 'allow(dead_code) from Blocks::count access getter method
- [ ] #9 Name all temp files in unit test's as its test.
- [ ] #11 Add strong mechanism for deleting all temp files after failed or successfull tests

- [x] #1 Add handling bad file open.
- [x] #2 For read_lines function. Add handling Result type here.
- [x] #3 Move this function to another module outside of main
- [x] #4 Add for Blocks read method wich can read from one file or many (from folder as example).
    - [x] #10 Adding read method to Blocks as wrapper.